import 'package:animate_do/animate_do.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:musicapp/src/helpers/helpers.dart';
import 'package:musicapp/src/models/audio_player_model.dart';
import 'package:musicapp/src/widgets/custom_bar_widget.dart';
import 'package:provider/provider.dart';
class MusicPlayerPage extends StatelessWidget {
  const MusicPlayerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Stack(
            children: [
              BackgroundApp(),
              Column(
              children: <Widget>[
                CustomAppBar(),
                ImageDiskDuration(),
                TitlePlay(),
                SizedBox(height: 64),
                Expanded(
                    child: ListMusicsViews()
                )
              ],
            ),]
          )
        ),
    );
  }
}

class BackgroundApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: screenSize.height *  0.8,
      decoration: const BoxDecoration(
        borderRadius:  BorderRadius.only(bottomLeft: Radius.circular(60)),
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.center,
          colors: [
            Color(0xff33333E),
            Color(0xff201E28)
          ]
        )
      ),
    );
  }
}

class ListMusicsViews extends StatelessWidget {

  final lyrics = getLyrics();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListWheelScrollView(
        physics:  BouncingScrollPhysics(),
        itemExtent: 42,
        diameterRatio:  1.5,
        children: lyrics.map(
            (line) => Text(line, style: TextStyle(fontSize: 20, color: Colors.white.withOpacity(0.6)))
        ).toList(),
      ),
    );
  }
}

class ImageDiskDuration extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      margin: const EdgeInsets.only(top: 70),
      child: Row(
        children: <Widget>[
          Spacer(),
          ImageDisk(),
          SizedBox(width: 30),
          ProgressBarMusic(),
          Spacer(),
        ],
      )
    );
  }
}

class ProgressBarMusic extends StatelessWidget {
  final txtStyle =  TextStyle(color: Colors.white.withOpacity(0.4));

  @override
  Widget build(BuildContext context) {
    final audioPlayerModel =  Provider.of<AudioPlayerModel>(context);
    final percentage =  audioPlayerModel.percentage;
    return Container(
      child: Column(
        children: <Widget>[
          Text('${audioPlayerModel.songTotalDuration}', style: txtStyle ),
          const SizedBox(height: 10),
          Stack(
            children: <Widget>[
              Container(
                width: 3,
                height: 230,
                color: Colors.white.withOpacity(0.1)
              ),
              Positioned(
                bottom: 0,
                child: Container(
                    width: 3,
                    height: 230 * percentage,
                    color: Colors.white.withOpacity(0.8)
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Text('${audioPlayerModel.currentSecond}', style: txtStyle ),
        ],
      ),
    );
  }
}

class ImageDisk extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final audioPlayerModel =  Provider.of<AudioPlayerModel>(context);
    return Container(
      padding: EdgeInsets.all(20),
      width: 230,
      height: 230,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(200),
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          colors: [
            Color(0xff484750),
            Color(0xff1e1c24)
          ]
        ),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(200),
        child: Stack(
          alignment: Alignment.center,
          children: [
            SpinPerfect(
                duration: Duration(seconds: 10),
                infinite: true,
                manualTrigger: true,
                controller: (animationController) => audioPlayerModel.controller = animationController,
                child: Image(image: AssetImage('assets/aurora.jpg'))
            ),
            Container(
              width: 25,
              height: 25,
              decoration: BoxDecoration(
                color: Colors.black38,
                borderRadius:  BorderRadius.circular(100)
              ),
            ),
            Container(
              width: 18,
              height: 18,
              decoration: BoxDecoration(
                  color: Color(0xff11C25),
                  borderRadius:  BorderRadius.circular(100)
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TitlePlay extends StatefulWidget {
  @override
  State<TitlePlay> createState() => _TitlePlayState();
}

class _TitlePlayState extends State<TitlePlay> with SingleTickerProviderStateMixin {
  bool isPlaying = false;
  bool firstTime = true;
  late AnimationController playAnimation;

  final assetAudioPlayer =  AssetsAudioPlayer();

  @override
  void initState() {
    playAnimation = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    super.initState();
  }

  @override
  void dispose() {
    playAnimation.dispose();
    super.dispose();
  }

  void open() async {

    final audioPlayerModel =  Provider.of<AudioPlayerModel>(context, listen: false);
    final assetsAudioPlayer = AssetsAudioPlayer();

    try {
      await assetsAudioPlayer.open(
        Audio("assets/Breaking-Benjamin-Far-Away.mp3"),
        autoStart: true,
        showNotification: true,
      );
    } catch (t) {
      print(t);
    }

    assetsAudioPlayer.current.listen((duration){
      audioPlayerModel.current = duration as Duration;
    });

    assetsAudioPlayer.current.listen((playingAudio) {
      audioPlayerModel.songDuration = playingAudio?.audio.duration ?? const Duration(seconds: 0);
    });
  }


  @override
  Widget build(BuildContext context) {

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40),
      margin: EdgeInsets.only(top: 40),
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Text('Far Away', style: TextStyle(fontSize: 30, color: Colors.white.withOpacity(0.8))),
              Text('-Breaking Benjamin-', style: TextStyle(fontSize: 15, color: Colors.white.withOpacity(0.5)))
            ],
          ),
          Spacer(),
          FloatingActionButton(
            elevation: 0,
            highlightElevation: 0,
            onPressed: () {
              final audioPlayerModel =  Provider.of<AudioPlayerModel>(context, listen: false);
              if(isPlaying){
                playAnimation.reverse();
                isPlaying = false;
                audioPlayerModel.controller.stop();
              }else{
                playAnimation.forward();
                isPlaying = true;
                audioPlayerModel.controller.repeat();
              }

              if(firstTime){
                open();
                firstTime = false;
              }else{
                assetAudioPlayer.playOrPause();
              }
            },
            backgroundColor:  const Color(0xffF8CB51),
            child: AnimatedIcon(
              icon: AnimatedIcons.play_pause,
              progress: playAnimation,
            ),

          )
        ],
      ),
    );
  }
}
