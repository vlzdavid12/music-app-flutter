import 'package:flutter/material.dart';
import 'package:musicapp/src/models/audio_player_model.dart';
import 'package:musicapp/src/pages/music_player_page.dart';
import 'package:musicapp/src/theme/theme.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create:  (_)=> AudioPlayerModel())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Music App',
        theme: myTheme,
        home: Scaffold(
          body: MusicPlayerPage(),
        ),
      ),
    );
  }
}


